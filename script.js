const WIDTH = 800;
const HEIGHT = 500;
const canvas = document.getElementById('canvas');
canvas.width = WIDTH;
canvas.height = HEIGHT;
const N = 1000;

const getContextCanvas = (value) => canvas.getContext(value);
const ctx = getContextCanvas('2d');

const randomInteger = (max, min = 0) => Math.floor(min + Math.random() * (max + 1 - min));

const getRectanglePoints = (handler, maxWidth, maxHeight) => {
    const x1 = randomInteger(maxWidth);
    const y1 = randomInteger(maxHeight);
    const x2 = randomInteger(maxWidth);
    const y2 = randomInteger(maxHeight);

    return {x1: x1, y1: y1, x2: x2, y2: y2};
};

const rectangle1 = getRectanglePoints(randomInteger, WIDTH, HEIGHT);
const rectangle2 = getRectanglePoints(randomInteger, WIDTH, HEIGHT);

const upgradeRectangleCoordinates = (rec) => {
    const rx1 = rec.x1 > rec.x2 ? rec.x2 : rec.x1;
    const rx2 = rec.x1 > rec.x2 ? rec.x1 : rec.x2;
    const ry1 = rec.y1 > rec.y2 ? rec.y2 : rec.y1;
    const ry2 = rec.y1 > rec.y2 ? rec.y1 : rec.y2;

    return {x1: rx1, y1: ry1, x2: rx2, y2: ry2};
};

const upgradedRectangle1 = upgradeRectangleCoordinates(rectangle1);
const upgradedRectangle2 = upgradeRectangleCoordinates(rectangle2);

const drawRect = (x, y, width, height, color) => {
    ctx.strokeStyle = color;
    ctx.strokeRect(x, y, width, height);
};

drawRect(upgradedRectangle1.x1, upgradedRectangle1.y1, upgradedRectangle1.x2 - upgradedRectangle1.x1, upgradedRectangle1.y2 - upgradedRectangle1.y1);
drawRect(upgradedRectangle2.x1, upgradedRectangle2.y1, upgradedRectangle2.x2 - upgradedRectangle2.x1, upgradedRectangle2.y2 - upgradedRectangle2.y1);

const intersectionPointsRectangle = (a, b) => {
    const left = Math.max(a.x1, b.x1);
    const top = Math.max(a.y1, b.y1);
    const right = Math.min(a.x2, b.x2);
    const bottom = Math.min(a.y2, b.y2);

    const width = right - left;
    const height = bottom - top;

    if ((width < 0) || (height < 0))
        return null;

    return {x1: left, y1: top, x2: right, y2: bottom};
};

const rectangle3 = intersectionPointsRectangle(upgradedRectangle1, upgradedRectangle2);

const drawEllipse = (x , y , radiusX , radiusY, color) => {
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.ellipse(x , y , radiusX , radiusY, Math.PI, 0, 2 * Math.PI);
    ctx.stroke();
};

const drawPoint = (x, y, color) => {
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.arc(x, y, 1, 0, 2 * Math.PI);
    ctx.stroke();
};

const generatePoints = (centerX, centerY, radiusX, radiusY) => {
    const t = 2 * Math.PI * Math.random();
    const d = Math.sqrt(Math.random());
    const x = centerX + radiusX * d * Math.cos(t);
    const y = centerY + radiusY * d * Math.sin(t);
    return ({x: x, y: y});
};

if(rectangle3 !== null) {
    const getEllipseCoordinates = (rect) => {
        const cx = ((rect.x2 - rect.x1) / 2) + rect.x1;
        const cy = ((rect.y2 - rect.y1) / 2) + rect.y1;
        const rx = (rect.x2 - rect.x1) / 2;
        const ry = (rect.y2 - rect.y1) / 2;
        return {cx: cx, cy: cy, rx: rx, ry: ry};
    };

    const ellipse = getEllipseCoordinates(rectangle3);

    drawRect(rectangle3.x1, rectangle3.y1, rectangle3.x2 - rectangle3.x1, rectangle3.y2 - rectangle3.y1, "red");

    drawEllipse(ellipse.cx, ellipse.cy, ellipse.rx, ellipse.ry, "green");

    const fillArr = (centerX, centerY, radiusX, radiusY, size, handler) => [...Array(size)].map(() => handler(centerX, centerY, radiusX, radiusY));

    const array = fillArr(ellipse.cx, ellipse.cy, ellipse.rx, ellipse.ry, N, generatePoints);

    const drawPointsEllipse = (arr, handler, color ) => arr.forEach(element => handler(element.x, element.y, color));

    drawPointsEllipse(array, drawPoint, "blue");

}
